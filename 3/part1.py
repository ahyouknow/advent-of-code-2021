import getInput

def main():
    inp = getInput.run()
    positionCount = createPositionCount(inp)
    for byte in inp:
        for bitIndex in range(len(byte)):
            if byte[bitIndex] == "0":
                positionCount[bitIndex]["0"]+=1
            else:
                positionCount[bitIndex]["1"]+=1

    gammaRate = ""
    epsilonRate = ""
    for bit in positionCount.keys():
        count = positionCount[bit]
        if count["0"] >= count["1"]:
            gammaRate = gammaRate + "0"
            epsilonRate = epsilonRate + "1"
        else:
            gammaRate = gammaRate + "1"
            epsilonRate = epsilonRate + "0"
    print(gammaRate)
    print(epsilonRate)
    intGammaRate = int(gammaRate, 2)
    intEpsilonRate = int(epsilonRate, 2)
    print("gamma rate: {}".format(intGammaRate))
    print("epsilon rate: {}".format(intEpsilonRate))
    print("answer: {}".format(intGammaRate * intEpsilonRate))

def createPositionCount(inp):
    bitCount = len(inp[0])
    output = {}
    for i in range(bitCount):
        output[i] = { "0": 0, "1":0 }
        
    return output
        
main()

