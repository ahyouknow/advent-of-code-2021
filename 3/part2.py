import getInput

def main():
    inp = getInput.run()
    OxyGenRatings = inp
    CO2GenRatings = inp
    for bitIndex in range(len(inp[0])):
        count = countBinaryList(OxyGenRatings, bitIndex)
        if count["1"] >= count["0"]:
            choose = "1"
        else:
            choose = "0"
        OxyGenRatings = [i for i in OxyGenRatings if i[bitIndex] == choose]
        if len(OxyGenRatings) == 1:
            break
    for bitIndex in range(len(inp[0])):
        count = countBinaryList(CO2GenRatings, bitIndex)
        if count["1"] >= count["0"]:
            choose = "0"
        else:
            choose = "1"
        CO2GenRatings = [i for i in CO2GenRatings if i[bitIndex] == choose]
        if len(CO2GenRatings) == 1:
            break
    print("Oxygen generator ratings: {}".format(OxyGenRatings))
    print("CO2 generator ratings: {}".format(CO2GenRatings))
    print("Answer: {}".format(int(OxyGenRatings[0],2) * int(CO2GenRatings[0], 2)))

def createPositionCount(inp):
    bitCount = len(inp[0])
    output = {}
    for i in range(bitCount):
        output[i] = { "0": 0, "1":0 }
        
    return output

def countBinaryList(inp, index):
    positionCount = { "0": 0, "1":0 }
    for byte in inp:
        if byte[index] == "0":
            positionCount["0"]+=1
        else:
            positionCount["1"]+=1
    return positionCount

main()

