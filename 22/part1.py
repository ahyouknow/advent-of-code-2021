import getInput
import itertools

inp = getInput.run()

func = lambda x: tuple(map(int, (x[2:].split(".."))))
onboxes = set()
for command in inp:
    state, lists = command.split(" ")
    x, y, z = list(map(func, lists.split(",")))
    if not all([i[0] >= -50 and i[1] <= 50 for i in (x, y, z)]):
        continue
    boxes = set(itertools.product(range(x[0],x[1]+1), range(y[0],y[1]+1), range(z[0],z[1]+1)))
    if state == "on":
        onboxes = onboxes | boxes
    if state == "off":
        onboxes = onboxes - (onboxes & boxes)

print(len(onboxes))

