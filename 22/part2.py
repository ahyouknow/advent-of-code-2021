import getInput
import itertools

class cube:
    def __init__(self, xlo, xhi, ylo, yhi, zlo, zhi):
        self.xlo = xlo
        self.xhi = xhi
        self.ylo = ylo
        self.yhi = yhi
        self.zlo = zlo
        self.zhi = zhi

    def getIntersect(self, box2):


inp = getInput.run()

func = lambda x: tuple(map(int, (x[2:].split(".."))))
onboxes = set()
offboxes = set()
for command in inp:
    state, lists = command.split(" ")
    x, y, z = list(map(func, lists.split(",")))
    boxes = set([(xn,yn,zn) for xn,yn,zn in zip(range(x[0],x[1]+1),range(y[0],y[1]+1),range(z[0],z[1]+1)) ])
    if state == "on":
        onboxes = onboxes | boxes
    if state == "off":
        offboxes = offboxes | boxes

print(onboxes)
print(offboxes)
