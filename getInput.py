#!/usr/bin/python

def run():
    file = open('input.txt', 'r')
    lines = file.read().strip().splitlines()
    file.close()
    return lines

if __name__ == '__main__':
    main()

