import getInput

class LaternFish:
    def __init__(self):
        self.memory = {}

    def laternFishRec(self, timer, days):
        if timer in self.memory.keys():
            return self.memory[timer]
        elif timer > days:
            return 1
        elif (timer+1) == days:
            return 2
        day = timer+1
        children = 0
        while (day <= days):
            children+=(self.laternFishRec(day+8, days))
            day+=7
        self.memory[timer] = children + 1
        return children + 1

def main():
    inp = getInput.run()
    timers = tuple(map(int, inp[0].split(",")))
    new = LaternFish()
    finalCount = 0
    for x in timers:
        finalCount+=new.laternFishRec(x, 256)

    print("answer: {}".format(finalCount))



main()
