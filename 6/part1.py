import getInput

class LaternFish:
    def __init__(self, timer):
        self.timer = timer
        self.children = []

    def countdown(self):
        for child in self.children:
            child.countdown()
        if self.timer == 0:
            self.createChild()
            self.timer = 7
        self.timer -= 1

    def createChild(self):
        newChild = LaternFish(8)
        self.children.append(newChild)

    def countPop(self):
        count = 0
        for child in self.children:
            count+=child.countPop()
        return 1 + count

def main():
    inp = getInput.run()
    timers = tuple(map(int, inp[0].split(",")))
    initFishes = []
    for timer in timers:
        initFishes.append(LaternFish(timer))

    for _ in range(80):
        for fish in initFishes:
            fish.countdown()

    finalCount = 0

    for fish in initFishes:
        print(len(fish.children))
        finalCount+=fish.countPop()

    print("answer: {}".format(finalCount))


main()

