import getInput
import numpy as np

def getNeighbors(grid, x, y):
    neighbors = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
    if x == 0:
        neighbors.remove((x-1, y))
    elif (x+1) == grid.shape[0]:
        neighbors.remove((x+1, y))
    if y == 0:
        neighbors.remove((x, y-1))
    elif (y+1) == grid.shape[1]:
        neighbors.remove((x, y+1))
    return neighbors


grid = np.array([list(map(int, list(x))) for x in getInput.run()])
lowPoints = []
for x in range(grid.shape[0]):
    for y in range(grid.shape[1]):
        neighbors = getNeighbors(grid, x, y)
        if all([grid[x][y] < grid[n_x][n_y] for n_x, n_y in neighbors]):
            lowPoints.append(grid[x][y])

print(f"answer: {sum(lowPoints) + len(lowPoints)}")

