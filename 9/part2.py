import getInput
import numpy as np

def getNeighbors(grid, x, y):
    neighbors = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
    if x == 0:
        neighbors.remove((x-1, y))
    elif (x+1) == grid.shape[0]:
        neighbors.remove((x+1, y))
    if y == 0:
        neighbors.remove((x, y-1))
    elif (y+1) == grid.shape[1]:
        neighbors.remove((x, y+1))
    return neighbors

def find9(grid, basins, x, y):
    neighbors = getNeighbors(grid, x, y)
    basin_neighbors = []
    for coord in neighbors:
        n_x = coord[0]
        n_y = coord[1]
        if coord not in basins and grid[n_x][n_y] != 9:
            basin_neighbors.append(coord)
    basins.extend(basin_neighbors)
    for n_x, n_y in basin_neighbors:
        basins = find9(grid, basins, n_x, n_y)
    return basins

grid = np.array([list(map(int, list(x))) for x in getInput.run()])
basinNumbers = []
for x in range(grid.shape[0]):
    for y in range(grid.shape[1]):
        basins = []
        neighbors = getNeighbors(grid, x, y)
        if all([grid[x][y] < grid[n_x][n_y] for n_x, n_y in neighbors]):
            basins.append(tuple([x,y]))
            basins = find9(grid, basins, x, y)
            basinNumbers.append(len(basins))

answer = (sorted(basinNumbers))[-3:]
print(f"answer: {answer[0] * answer[1] * answer[2]}")

