import getInput

def main():
    inp = getInput.run()
    inp = list(map(splitElements, inp))
    trace = {}
    for action in inp:
        if action[0][0] == action[1][0]:
            static = action[0][0]
            direction = int((action[1][1]-action[0][1])/abs(action[1][1]-action[0][1]))
            steps = range(action[0][1], action[1][1]+direction, direction)
            tracelist = zip([static for _ in steps], steps)
        elif action[0][1] == action[1][1]:
            static = action[0][1]
            direction = int((action[1][0]-action[0][0])/abs(action[1][0]-action[0][0]))
            steps = range(action[0][0], action[1][0]+direction, direction)
            tracelist = zip(steps, [static for _ in steps])
        else:
            xdirection = int((action[1][0]-action[0][0])/abs(action[1][0]-action[0][0]))
            ydirection = int((action[1][1]-action[0][1])/abs(action[1][1]-action[0][1]))
            xsteps = range(action[0][0], action[1][0]+xdirection, xdirection)
            ysteps = range(action[0][1], action[1][1]+ydirection, ydirection)
            tracelist = zip(xsteps, ysteps)

        for step in tracelist:
            if step in trace.keys():
                trace[step]+=1
            else:
                trace[step] = 1
    answer = trace.values()
    answer = [ x for x in answer if x > 1 ]
    print("answer: {}".format(len(answer)))



def splitElements(x):
    out = x.split(" -> ")
    out = tuple(map(int, out[0].split(","))), tuple(map(int, out[1].split(",")))
    return out

main()
