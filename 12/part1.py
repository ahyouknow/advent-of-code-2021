import getInput

def pathRec(location, paths, visited):
    if location == 'end':
        return 1
    if location.lower() == location:
        visited.append(location)
    pathCount = 0
    for path in paths[location]:
        if path in visited:
            continue
        pathCount+=pathRec(path, paths, visited.copy())
    return pathCount


inp = getInput.run()
paths = {}
for path in inp:
    a, b = path.split("-")
    if a in paths.keys():
        paths[a].append(b)
    else:
        paths[a] = [b]
    if b in paths.keys():
        paths[b].append(a)
    else:
        paths[b] = [a]

answer = pathRec('start', paths, [])
print(f"answer: {answer}")
