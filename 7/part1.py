import getInput

inp = getInput.run()
positions = list(map(int, inp[0].split(',')))
positions.sort()
median = positions[int(len(positions)/2)]
fuel = 0
for position in positions:
    fuel+=abs(position-median)

print(fuel)
