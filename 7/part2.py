import getInput
import math

inp = getInput.run()
positions = list(map(int, inp[0].split(',')))
ceil_mean = math.ceil(sum(positions) / len(positions))
floor_mean = math.floor(sum(positions) / len(positions))
ceil_fuel = 0
floor_fuel = 0
for position in positions:
    ceil_difference = abs(ceil_mean - position)
    ceil_fuel+=sum(range(0, ceil_difference+1))

    floor_difference = abs(floor_mean - position)
    floor_fuel+=sum(range(0, floor_difference+1))

print(min([ceil_fuel, floor_fuel]))
