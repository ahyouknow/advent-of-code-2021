import getInput

def main():
    f = list(map(int, getInput.run()))
    counter = 0
    lastSum = f[0] + f[1] + f[2]
    for x in range(3, len(f)):
        thisSum = f[x-2] + f[x-1] + f[x]
        if thisSum > lastSum:
            counter+=1
        lastSum = thisSum
    print(counter)

main()
