#!/usr/bin/python
import getInput

#def getInput():
#    file = open('input1.txt', 'r')
#    lines = file.read().splitlines()
#    file.close()
#    return lines

def main():
    f = list(map(int, getInput.run()))
    counter = 0
    last = int(f[0])
    for x in f[1:]:
        if x > last:
            counter+=1
        last = x
    print(counter)
    
main()
