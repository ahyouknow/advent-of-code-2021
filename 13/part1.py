import getInput

inp = getInput.run()
dots = []
maxN = [0,0]
x = inp[0]
count=0
while (x != ''):
    x, y = map(int, x.split(','))
    dots.append([x,y])
    if x > maxN[0]:
        maxN[0] = x
    if y > maxN[1]:
        maxN[1] = y
    count+=1
    x = inp[count]

def cleanUpFold(command):
    out = command.replace('fold along ', '').split('=')
    if out[0] == 'x':
        out[0] = 0
    else:
        out[0] = 1
    out[1] = int(out[1])
    return tuple(out)

folds = list(map(cleanUpFold, inp[count+1:]))

fold = folds[0]
direction, length = fold
newdots = []
for dot in dots:
    if dot[direction] == length:
        continue
    elif dot[direction] > length:
        dot[direction] = abs(dot[direction] % -length)
    if dot not in newdots:
        newdots.append(dot)

print(f"answer: {len(newdots)}")
