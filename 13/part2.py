import getInput
import numpy as np
import sys
np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(linewidth=np.inf)

inp = getInput.run()
dots = []
maxN = [0,0]
x = inp[0]
count=0
while (x != ''):
    x, y = map(int, x.split(','))
    dots.append([x,y])
    if x > maxN[0]:
        maxN[0] = x
    if y > maxN[1]:
        maxN[1] = y
    count+=1
    x = inp[count]

def cleanUpFold(command):
    out = command.replace('fold along ', '').split('=')
    if out[0] == 'x':
        out[0] = 0
    else:
        out[0] = 1
    out[1] = int(out[1])
    return tuple(out)

folds = list(map(cleanUpFold, inp[count+1:]))

for fold in folds:
    direction, length = fold
    newdots = []
    for dot in dots:
        if dot[direction] == length:
            continue
        elif dot[direction] > length:
            dot[direction] = abs(dot[direction] % -length)
        if dot not in newdots:
            newdots.append(dot)
    maxN[direction] = length - 1
    dots = newdots
pic = np.empty((maxN[0]+1, maxN[1]+1),dtype=str)
pic.fill('.')
for x,y in dots:
    pic[x][y] = '#'

print(np.transpose(pic))

