import getInput

def main():
    inp = list(map(splitinput, getInput.run()))
    aim = 0
    depth = 0
    horPos = 0 
    for command in inp:
        if command[0] == "forward":
            horPos+=command[1]
            depth = depth + (command[1] * aim)
        elif command[0] == "down":
            aim+=command[1]
        elif command[0] == "up":
            aim-=command[1]

    print("depth {}, horizontal position {}".format(depth, horPos))
    print("Answer = {}".format(depth * horPos))

def splitinput(inp):
    out = inp.split(" ")
    out[1] =  int(out[1])
    return tuple(out)

main()
