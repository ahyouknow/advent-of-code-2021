import getInput

def main():
    inp = list(map(splitinput, getInput.run()))
    depth = 0
    horPos = 0 
    for command in inp:
        if command[0] == "forward":
            horPos+=command[1]
        elif command[0] == "down":
            depth+=command[1]
        elif command[0] == "up":
            depth-=command[1]

    print("depth {}, horizontal position {}".format(depth, horPos))
    print("Answer = {}".format(depth * horPos))



def splitinput(inp):
    out = inp.split(" ")
    out[1] =  int(out[1])
    return tuple(out)

main()
