import getInput

class board:
    def __init__(self):
        self.win = False
        self.board = []
        self.bingo = []
        self.sum = 0
        for _ in range(5):
            self.bingo.append([0,0,0,0,0])

    def addBoard(self, boardinput):
        for i in range(len(boardinput)):
            self.board.append(list((map(int, boardinput[i].split()))))
            self.sum+=sum(self.board[i])

    def call(self, number):
        for i in range(len(self.board)):
            row = self.board[i]
            if number in row:
                j = row.index(number)
                self.bingo[i][j] = 1
                self.sum-=number

    def checkWin(self):
        colCheck = [0, 1, 2, 3, 4]
        for i in range(len(self.bingo)):
            if self.bingo[i].count(1) >= 5:
                self.win = True
                return
            copyCheck = colCheck.copy()
            for j in copyCheck:
                if self.bingo[i][j] == 0:
                    colCheck.remove(j)
        if len(colCheck) > 0:
            self.win = True

def main():
    inp = getInput.run()
    inp = [x for x in inp if x != ""]
    print(inp)
    calls = list(map(int, inp[0].split(',')))
    inp.pop(0)
    boards = []
    currentboard = []
    print(len(inp))
    for i in range(0,len(inp), 5):
        currentboards = []
        newboard = board()
        boards.append(newboard)
        for j in range(i, i+5):
            currentboards.append(inp[j])
        newboard.addBoard(currentboards)
    winningList = getWinner(calls, boards)
    winningSum = winningList[1].sum
    winningNumber = winningList[0]

    print("answer: {}".format(winningSum*winningNumber))


def getWinner(calls, boards):
    for number in calls:
        for b in boards:
            b.call(number)
            b.checkWin()
            print(b.board)
            if b.win:
                print(b.board)
                print(b.bingo)
                winningNumber = number
                winner = b
                print(b.sum)
                print(winningNumber)
                return [winningNumber, winner]
main()
