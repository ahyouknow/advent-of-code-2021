import getInput

inp = getInput.run()
signals = [y for x in inp for y in x.split(" | ")[1].split(" ") ]
print(signals)
count = 0
for signal in signals:
    length = len(signal)
    if length == 2 or length == 3 or length == 4 or length == 7:
        print(signal)
        count+=1

print(count)

