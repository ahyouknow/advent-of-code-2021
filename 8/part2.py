import getInput

rows = getInput.run()
answer = 0
for row in rows:
    signals, output = row.split(" | ")
    signal_patterns = {len(x): set(x) for x in signals.split()}
    one  = signal_patterns[2]
    four = signal_patterns[4]
    outputs = [set(x) for x in output.split()]
    out_number = ""
    for output in outputs:
        match (len(output), len(output & one), len(output & four)):
            case (2,_,_): out_number+='1'
            case (3,_,_): out_number+='7'
            case (4,_,_): out_number+='4'
            case (7,_,_): out_number+='8'
            case (5,2,_): out_number+='3'
            case (5,1,2): out_number+='2'
            case (5,1,3): out_number+='5'
            case (6,2,3): out_number+='0'
            case (6,1,3): out_number+='6'
            case (6,2,4): out_number+='9'
    answer+=int(out_number)

print(f"answer: {answer}")
