import getInput
import math

inp = getInput.run()
corruptedStrings = [
        "(]",
        "{()()()>",
        "(((()))}",
        "<([]){()}[{}])"
        ]

legal = {
        "{":"}",
        "(":")",
        "[":"]",
        "<":">",
        }
expValue = {
        ")":1,
        "]":2,
        "}":3,
        ">":4,
        }

scores = []
for line in inp:
    if not all([x not in line for x in corruptedStrings]):
        continue
    expected = []
    corrupted = False
    score = 0
    for char in line:
        if char in legal.keys():
            expected.append(legal[char])
        elif char == expected[-1]:
            expected.pop()
        else:
            corrupted = True
            break
    if not corrupted:
        for x in reversed(expected):
            score = (score * 5) + expValue[x]
        scores.append(score)

answer = list(sorted(scores))[math.floor(len(scores)/2)]
print(f"answer: {answer}")
