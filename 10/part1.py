import getInput

inp = getInput.run()
corrupted = [
        "(]",
        "{()()()>",
        "(((()))}",
        "<([]){()}[{}])"
        ]

legal = {
        "{":"}",
        "(":")",
        "[":"]",
        "<":">",
        }
illegalValue = {
        ")":3,
        "]":57,
        "}":1197,
        ">":25137,
        }

answer = 0
for line in inp:
    expected = []
    if not all([x not in line for x in corrupted]):
        continue
    for char in line:
        if char in legal.keys():
            expected.append(legal[char])
        elif char == expected[-1]:
            expected.pop()
        else:
            answer+=(illegalValue[char])
            break

print(f"answer: {answer}")



